var ALFABETO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var TABULEIRO = null;
var PALAVRAS = [];
var PALAVRAS_BUSCA = {};
var quadrados_podem_ser_clicados = [];
var tempo = 0;
var TOTAL_PALAVRAS = 0
var intervaloPlacar = null;

function atualizaPlacar()
{
    if(TOTAL_PALAVRAS === 0)
    {
        clearInterval(intervaloPlacar);
        return ;
    }
    tempo++;
    var segundos = tempo % 60;
    var minutos = Math.floor((tempo % (60 * 60)) / 60);
    var horas = Math.floor((tempo % (60 * 60 * 60)) / 3600);
    if(horas != 0)
    {
        texto = segundos + ":" + minutos +  ":" + horas;
    }
    else if(minutos != 0)
    {
        texto = segundos + ":" + minutos
    }
    else
    {
        texto = segundos;
    }
    placar.innerHTML = texto;
    
}

function pontoInicial(json)
{
    palavras_array = parseiaJSON(json);
    TOTAL_PALAVRAS = palavras_array.length;
    iniciaJogo(palavras_array);
}

function iniciaJogo(palavras)
{
    TABULEIRO = jsboard.board({attach: "game", size: TAMANHO+"x"+TAMANHO});
    PALAVRAS = palavras;
    var placar = document.getElementById("placar");
    var palavrasFaltando = document.getElementById("palavrasFaltando");
    palavrasFaltando.innerHTML = TOTAL_PALAVRAS;
    intervaloPlacar = setInterval(atualizaPlacar, 1000);
    logicaJogo();
}

/*
fetch(window.location.href, {method: 'POST'}).then(function(resposta) {
    resposta.json().then(function(resultado) {
        
        pontoInicial(resultado);
    });
});
*/


function parseiaJSON(json)
{
    palavras = []
    for(var chave in json)
    {
        obj = json[chave];
        palavra = []
        palavra.push(obj["iniciais"])
        palavra.push(obj["finais"]);
        palavra.push(obj["palavra"]);
        palavras.push(palavra);
    }
    return palavras;
}

function criaPeca(letra)
{
    var peca = jsboard.piece({text: letra, fontSize: "40px", textAlign: "center"});
    return peca
}

function coloreQuadrado(item, peca, classe)
{
    TABULEIRO.cell(item).place(peca.clone(classe));
    TABULEIRO.cell(item).DOM().classList = TABULEIRO.cell(item).DOM().classList[0];
    void TABULEIRO.cell(item).DOM().offsetWidth;
    TABULEIRO.cell(item).DOM().classList.add(classe);
}

function checaSelecao(selecao)
{
    var inicio = selecao[0];
    var fim = selecao[selecao.length -1];
    if(inicio[0] in PALAVRAS_BUSCA)
    {
        if(inicio[1] in PALAVRAS_BUSCA[inicio[0]])
        {
            palavra = PALAVRAS_BUSCA[inicio[0]][inicio[1]];
            return palavra[1][0] === fim[0] && palavra[1][1] === fim[1];
        }
    }
    else if(fim[0] in PALAVRAS_BUSCA)
    {
        if(fim[1] in PALAVRAS_BUSCA[fim[0]])
        {
            palavra = PALAVRAS_BUSCA[fim[0]][fim[1]];
            return palavra[1][0] === inicio[0] && palavra[1][1] === inicio[1];
        }
    }
    return false;
}

function coloreSelecao(selecao, classe)
{
    selecao.forEach(function(item, index) {
        var letra = TABULEIRO.cell(item).get();
        var peca = jsboard.piece({text: letra, fontSize: "40px", textAlign: "center"});
        coloreQuadrado(item, peca, classe);
    });
}

function identificaPecasComoInclicaveis(pecas)
{
    pecas.forEach(function(item, index) {
        quadrados_podem_ser_clicados[item[0]][item[1]] = false;
    });
}

function checaColisao(selecao)
{
    var colisao = false;
    selecao.forEach(function(item, index) {
        if(!quadrados_podem_ser_clicados[item[0]][item[1]])
        {
            colisao = true;
        }
    });
    return colisao;
}

function logicaJogo()
{
    // Preenche tabuleiro inicialmente com letras aleatorias
    for(var i = 0; i < TAMANHO; i ++)
    {
        quadrados_podem_ser_clicados[i] = []
        for(var j = 0; j < TAMANHO; j++)
        {
            var letra = ALFABETO.charAt(Math.floor(Math.random() * 26));
            var peca = criaPeca(letra);
            TABULEIRO.cell([i, j]).DOM().style.removeProperty("background-color");
            coloreQuadrado([i, j], peca, "cor-inicial");
            quadrados_podem_ser_clicados[i][j] = true;
        }
    }
    // Bota as palavras no tabuleiro
    PALAVRAS.forEach(function(item, index){
        if(PALAVRAS_BUSCA[item[0][0]] === undefined)
        {
            PALAVRAS_BUSCA[item[0][0]] = {};
        }
        PALAVRAS_BUSCA[item[0][0]][item[0][1]] = item;
        var inicio = item[0];
        var fim = item[1];
        var letras = item[2];
        var distancia_y = fim[0] - inicio[0];
        var distancia_x = fim[1] - inicio[1];
        for(var i = 0; i <= Math.max(distancia_x, distancia_y); i++)
        {
            var peca = criaPeca(letras.charAt(i));
            coloreQuadrado([inicio[0] + Math.min(distancia_y, i), inicio[1] + Math.min(distancia_x, i)], peca, "cor-inicial");
        }
    });

    coordenadas_peca_clicada = null;

    // implementa logica para o clique em cada peca
    TABULEIRO.cell("each").on("click", function() {
        var coordenadas = TABULEIRO.cell(this).where();
        if(quadrados_podem_ser_clicados[coordenadas[0]][coordenadas[1]])
        {
            if(coordenadas_peca_clicada === null)
            {
                var letra = TABULEIRO.cell(this).get();
                var peca = criaPeca(letra);
                coloreQuadrado(this, peca, "cor-temporaria");
                coordenadas_peca_clicada = TABULEIRO.cell(this).where();
            }
            else{
                // peca é a mesma que já foi selecionada, deseleciona ela
                if(coordenadas[0] === coordenadas_peca_clicada[0] && coordenadas[1] === coordenadas_peca_clicada[1])
                {
                    var peca = criaPeca(TABULEIRO.cell(this).get());
                    coloreQuadrado(this, peca, "cor-inicial");
                    coordenadas_peca_clicada = null;
                }
                else
                {   
                    var menor_x = Math.min(coordenadas[0], coordenadas_peca_clicada[0]);
                    var maior_x = Math.max(coordenadas[0], coordenadas_peca_clicada[0]);
                    var menor_y = Math.min(coordenadas[1], coordenadas_peca_clicada[1]);
                    var maior_y = Math.max(coordenadas[1], coordenadas_peca_clicada[1]);
                    var pecas_iluminadas = [];
                    // peca clicada está na mesma linha que a peca clicada anteriormente
                    if(coordenadas[0] === coordenadas_peca_clicada[0])
                    {
                        for(var j = menor_y; j <= maior_y; j++)
                        {
                            var coordenadas_intermediarias = [coordenadas[0], j];
                            pecas_iluminadas.push(coordenadas_intermediarias);
                        }
                        coordenadas_peca_clicada = null;
                        
                    }
                    // peca clicada está na mesma coluna que a peca clicada anteriormente
                    else if(coordenadas[1] === coordenadas_peca_clicada[1])
                    {
                        for(var i = menor_x; i <= maior_x; i++)
                        {
                            var coordenadas_intermediarias = [i, coordenadas[1]];
                            pecas_iluminadas.push(coordenadas_intermediarias);
                        }

                        coordenadas_peca_clicada = null;
                    }
                    // peca clicada está na diagonal da peca clicada anteriormente
                    else if(Math.abs(coordenadas[0] - coordenadas_peca_clicada[0]) === Math.abs(coordenadas[1] - coordenadas_peca_clicada[1]))
                    {
                        if(coordenadas[0] > coordenadas_peca_clicada[0])
                        {
                            mudanca_y = -1;
                        }
                        else
                        {
                            mudanca_y = 1;
                        }
                        if(coordenadas[1] > coordenadas_peca_clicada[1])
                        {
                            mudanca_x = -1
                        }
                        else
                        {
                            mudanca_x = 1;
                        }
                        passo_y = 0;
                        passo_x = 0
                        while(coordenadas[0] + passo_y != coordenadas_peca_clicada[0] + mudanca_y)
                        {
                            var coordenadas_intermediarias = [coordenadas[0] + passo_y, coordenadas[1] + passo_x];
                            pecas_iluminadas.push(coordenadas_intermediarias);
                            passo_y += mudanca_y;
                            passo_x += mudanca_x
                        }

                        coordenadas_peca_clicada = null
                    }
                    // peca nao pode fazer selecao com peca anterior, peca atual se torna peca anterior
                    else
                    {
                        var antiga_peca_clicada = criaPeca(TABULEIRO.cell(coordenadas_peca_clicada).get());
                        coloreQuadrado(coordenadas_peca_clicada, antiga_peca_clicada, "cor-inicial");
                        var peca_nova = criaPeca(TABULEIRO.cell(coordenadas).get());
                        coloreQuadrado(coordenadas, peca_nova, "cor-temporaria");
                        coordenadas_peca_clicada = coordenadas;
                    }
                    if(pecas_iluminadas.length > 0)
                    {
                        if(!checaColisao(pecas_iluminadas))
                        {
                            if(checaSelecao(pecas_iluminadas))
                            {
                                coloreSelecao(pecas_iluminadas, "correto");
                                identificaPecasComoInclicaveis(pecas_iluminadas);
                                TOTAL_PALAVRAS--;
                                palavrasFaltando.innerHTML = TOTAL_PALAVRAS;
                                if(TOTAL_PALAVRAS === 0)
                                {
                                    palavrasFaltando.classList.add("placarTerminou");
                                }
                            }
                            else
                            {
                                coloreSelecao(pecas_iluminadas, "corErrada");
                            }
                        }
                        else
                        {
                            peca = criaPeca(TABULEIRO.cell(pecas_iluminadas[pecas_iluminadas.length - 1]).get());
                            coloreQuadrado(pecas_iluminadas[pecas_iluminadas.length - 1], peca, "cor-inicial");
                            peca = criaPeca(TABULEIRO.cell(pecas_iluminadas[0]).get());
                            coloreQuadrado(pecas_iluminadas[0], peca, "cor-inicial");
                            antiga_peca_clicada = null;
                        }
                    }
                }
            }
        }
    });
}

pontoInicial(palavras);