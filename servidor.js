var express = require('express'),  app = express();
var path = require("path");
var fs = require('fs');

// Põe a pasta /public dentro do projeto como acessível
app.use(express.static(__dirname + '/public'));

// API para pegar as palavras do servidor
app.post('/', function(req, res) {
    var obj = JSON.parse(fs.readFileSync(__dirname + '/public/palavras.json', 'utf8'));
    res.json(obj);
    res.send();
});

// API para mandar o arquivo HTML principal
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + 'index.html'));
});

// Inicia o servidor na porta 8080, acessível digitando localhost:8080 no navegador quando servidor for rodado no node
app.listen(8080, function(){
    console.log("Escutando na porta 8080");
});